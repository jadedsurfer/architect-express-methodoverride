"use strict";
var middleware = require("method-override");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:methodoverride");
  debug("start");

  debug(".useSetup");
  imports.express.useSetup(middleware());

  debug("register nothing");
  register(null, {});
};
